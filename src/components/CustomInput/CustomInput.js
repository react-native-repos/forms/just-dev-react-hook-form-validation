import React from 'react';
import {View, TextInput, StyleSheet, Text} from 'react-native';
import {Controller} from 'react-hook-form';

// const CustomInput = ({value, setValue, placeholder, secureTextEntry}) => {
const CustomInput = ({
  control,
  name,
  rules = {}, // assign rules as empty object incase we dont pass any rules in
  placeholder,
  secureTextEntry,
}) => {
  /* comparing the text input and what value it has with the below text input controller */

  /* <TextInput
        value={value}
        setValue={setValue}
        onChange={setValue}
        placeholder={placeholder}
        style={styles.input}
        secureTextEntry={secureTextEntry}
      /> */

  /* Controller has 3 important properties:
        1) control function - first step to connect our text input with our form
        2) name - name of the field we are managing
        3) render method - helps us render the actual text input - binding of value and onChange text with the form
        receives certain properties - field: object which has other properties
        */

  return (
    <Controller
      control={control}
      name={name}
      rules={rules}
      // name="username"
      // rules={{required: true}} // makes sure text input isnt null, if it is it will throw an error, we can receive errors from the use form hook
      render={({field: {value, onChange, onBlur}, fieldState: {error}}) => (
        <>
          <View
            style={[
              styles.container,
              {borderColor: error ? 'red' : '#e8e8e8'},
            ]}>
            <TextInput
              // placeholder="username"
              placeholder={placeholder}
              value={value}
              onChangeText={onChange}
              onBlur={onBlur}
              style={styles.input}
              secureTextEntry={secureTextEntry}
            />
          </View>
          {error && (
            <Text style={{color: 'red', alignSelf: 'stretch'}}>
              {error.message || 'Error'}
            </Text>
          )}
        </>
      )}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    width: '100%',

    borderColor: '#e8e8e8',
    borderWidth: 1,
    borderRadius: 5,

    paddingHorizontal: 10,
    paddingTop: 10,
    paddingBottom: 10,
    marginVertical: 5,
  },
  input: {},
});

export default CustomInput;
