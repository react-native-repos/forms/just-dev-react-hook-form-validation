import React, {useState} from 'react';
import {
  View,
  Image,
  StyleSheet,
  useWindowDimensions,
  ScrollView,
} from 'react-native';
import Logo from '../../../assets/images/Logo_1.png';
import CustomButton from '../../components/CustomButton';
import CustomInput from '../../components/CustomInput';
import SocialSignInButtons from '../../components/SocialSignInButtons';
import {useNavigation} from '@react-navigation/native';
import {useForm} from 'react-hook-form';

// we use the Controller to connect the text input with the useForm
const SignInScreen = () => {
  // const [username, setUsername] = useState('');
  // const [password, setPassword] = useState('');

  const {height} = useWindowDimensions();
  const navigation = useNavigation();

  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm();

  console.log(errors);

  const onSignInPressed = data => {
    console.log(data);
    console.warn('Sign In');
    // validate user

    navigation.navigate('Home');
  };

  const onSignUpPressed = () => {
    console.warn('Sign Up');
    // validate user

    navigation.navigate('SignUp');
  };

  const onForgotPasswordPressed = () => {
    console.warn('forgot password');
    // validate user

    navigation.navigate('ForgotPassword');
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.root}>
        <Image
          source={Logo}
          // 30% of screen
          style={[styles.logo, {height: height * 0.3}]}
          resizeMode="contain"
        />
        <CustomInput
          name="username"
          placeholder="Username"
          control={control}
          rules={{required: 'Username is required'}}
          // rules={{required: true}}
          // value={username}
          // setValue={setUsername}
        />
        <CustomInput
          name="password"
          placeholder="Password"
          secureTextEntry={true}
          control={control}
          rules={{
            required: 'Password is required',
            minLength: {
              value: 3,
              message: 'Password should be minimum of 5 characters long',
            },
          }}
          // rules={{required: 'Password is required', minLength: 5}}
          // rules={{required: true}}
          // value={password}
          // setValue={setPassword}
        />

        {/* <Controller
          control={control}
          name="username"
          render={({field: {value, onChange, onBlur}}) => (
            <TextInput
              placeholder="username"
              value={value}
              onChangeText={onChange}
              onBlur={onBlur}
            />
          )}
        /> */}

        {/* before calling onSignInPressed and sending data to a server, we wrap it in handleSubmit - this will first validate the fields, make sure all the fields are valid first */}
        <CustomButton text="Sign in" onPress={handleSubmit(onSignInPressed)} />
        <CustomButton
          text="Forgot password?"
          onPress={onForgotPasswordPressed}
          type="TERTIARY"
        />

        <SocialSignInButtons />

        <CustomButton
          text="Don't have an account? Create one"
          onPress={onSignUpPressed}
          type="TERTIARY"
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  root: {
    alignItems: 'center',
    padding: 20,
  },
  logo: {
    width: '70%',
    maxWidth: 300,
    maxHeight: 200,
  },
});

export default SignInScreen;
