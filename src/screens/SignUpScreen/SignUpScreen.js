import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import CustomButton from '../../components/CustomButton';
import CustomInput from '../../components/CustomInput';
import SocialSignInButtons from '../../components/SocialSignInButtons';
import {useForm} from 'react-hook-form';

const EMAIL_REGEX = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

const SignUpScreen = () => {
  // const [username, setUsername] = useState('');
  // const [email, setEmail] = useState('');
  // const [password, setPassword] = useState('');
  // const [passwordRepeat, setPasswordRepeat] = useState('');
  const {control, handleSubmit, watch} = useForm({
    defaultValues: {
      username: 'Default username',
    },
  });
  const pwd = watch('password'); // provide name field of password where trying to watch, this will contain the value of our password
  const navigation = useNavigation();

  const onRegisterPressed = () => {
    console.warn('Register');
    // validate
    navigation.navigate('ConfirmEmail');
  };

  const onSignInPressed = () => {
    console.warn('Sign Up');
    // validate
    navigation.navigate('SignIn');
  };

  const onTermsOfUsePressed = () => {
    console.warn('on terms of us pressed');
  };

  const onPrivacyPressed = () => {
    console.warn('on privacy pressed');
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.root}>
        <Text style={styles.title}>Create an account</Text>
        {/* <CustomInput
          placeholder="Username"
          value={username}
          setValue={setUsername}
        /> */}
        <CustomInput
          name="username"
          control={control}
          placeholder="Username"
          rules={{
            required: 'Username is required',
            minLength: {
              value: 5,
              message: 'Username should be atleast 5 characters long',
            },
            maxLength: {
              value: 24,
              message: 'Username is maximum of 24 characters long',
            },
          }}
        />
        <CustomInput
          name="email"
          control={control}
          placeholder="Email"
          rules={{pattern: {value: EMAIL_REGEX, message: 'Email is invalid'}}}
        />
        <CustomInput
          name="password"
          control={control}
          placeholder="Password"
          secureTextEntry={true}
          rules={{
            required: 'Password is required',
            minLength: {
              value: 5,
              message: 'Password should be atleast 5 characters long',
            },
          }}
        />
        <CustomInput
          name="password-repeat"
          control={control}
          placeholder="Password Repeat"
          secureTextEntry={true}
          rules={{
            validate: value => value === pwd || 'Password do not match',
          }}
          // rules={{
          //   validate: value => value.length < 5 || 'Password do not match',
          // }}
          // rules={{
          //   validate: value =>
          //     value.length < 5 ? true : 'Password do not match',
          // }}
        />
        <CustomButton
          text="Register"
          onPress={handleSubmit(onRegisterPressed)}
        />
        <Text>
          By registering, you confirm that you accept our
          <Text style={styles.link} onPress={onTermsOfUsePressed}>
            {' '}
            Terms of Use
          </Text>{' '}
          and{' '}
          <Text style={styles.link} onPress={onPrivacyPressed}>
            Privacy Policy
          </Text>
        </Text>

        <SocialSignInButtons />

        <CustomButton
          text="Have an account? Sign in"
          onPress={onSignInPressed}
          type="TERTIARY"
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  root: {
    alignItems: 'center',
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#051C60',
    margin: 10,
  },
  text: {
    color: 'gray',
    marginVertical: 10,
  },
  link: {
    color: '#FDB075',
  },
});

export default SignUpScreen;
